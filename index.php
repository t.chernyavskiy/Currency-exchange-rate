<?php
// Ссылка на wsdl ЦБ РФ
const CB_WSDL = "http://www.cbr.ru/DailyInfoWebServ/DailyInfo.asmx?WSDL";
// Дата для запроса курса валют
$date["On_date"] = date("Y-m-d",time());

// Создание SOAP клиента
$client = new SoapClient(CB_WSDL);

//Получение данных от веб службы
$result = ($client) ? $client->GetCursOnDateXML($date) : die("Не удалось соедениться с сервером ЦБ РФ");

//Парсинг ответа XML
$sxml = new SimpleXMLElement($result->GetCursOnDateXMLResult->any);
?>
<ul>
    <?php
    foreach ($sxml->ValuteCursOnDate as $v) {
        echo "<li>".trim($v->Vname) . " | " . $v->Vnom ." ".$v->VchCode ." - " . $v->Vcurs . " RUB</li>";
    }
    ?>
</ul>